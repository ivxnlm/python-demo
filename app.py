from flask import Flask,jsonify,request

app = Flask (__name__)

from product import products

@app.route('/ping')
def ping():
    return jsonify({"message":"pong!"})

@app.route('/products',methods=['GET'])
def getProducts():
    return jsonify({"products":products})

# get Products
@app.route('/products/<string:product_name>',methods=['GET'])
def getProduct(product_name):
    print(product_name)
    productsFound = [product for product in products if product['name'] == product_name]
    if(len(productsFound) > 0 ):
        return jsonify({"product":productsFound[0]})
    else:
        return jsonify({"message":"product not found"})

# addProduct
@app.route('/products',methods=['POST'])
def addProduct():
    new_product = {
        "name":request.json['name'],
        "price":request.json['price'],
        "quantity":request.json['quantity']
    }
    products.append(new_product)
    print(request.json)
    return jsonify({"message":"Product added succesfully","products":products})

#updateProduct
@app.route('/products/<string:product_name>',methods=['PUT'])
def editProduct(product_name):
    productFound = [product for product in products if product['name'] == product_name]
    if(len (productFound) > 0):
        productFound[0]['name'] = request.json['name']
        productFound[0]['price'] = request.json['price']
        productFound[0]['quantity'] = request.json['quantity']
        return jsonify({"message":"Product Update succesfully","product":productFound[0]})
    else:
        return jsonify({"message": "Product not found"})

#deleteProduct
@app.route('/products/<string:product_name>',methods=['DELETE'])
def deleteProduct(product_name):
    productFound = [product for product in products if product['name'] == product_name]
    if(len (productFound) > 0):
        products.remove(productFound[0])
        return jsonify({"message":"Product Delete","products":products})
    else:
        return jsonify({"message": "Product not found"})


if __name__ == '__main__':
    app.run(debug=True,port=4000)
